/**
 * Created by admin on 28.10.2014.
 */
abstract public class Vehicle implements Driveable{
    public void turn(String direction){}
    public void accelerate(){}
    public void stop(){}
}
