/**
 * Created by admin on 28.10.2014.
 */
 public class SolarBattery implements EnergyProvider {
    private int electrisityVolume;

    public SolarBattery(int energy){
        electrisityVolume = energy;
    }

    @Override
    public int getEnergy(int energy){

        if(electrisityVolume - energy > 0){
            electrisityVolume -= energy;
        } else {
            electrisityVolume = 0;
        }

        return  energy;
    }

    int getElectrisityVolume(){
        return electrisityVolume;
    }
}
