/**
 * Created by admin on 28.10.2014.
 */
public class Car extends Vehicle {
    private DieselEngine dieselEngine;
    private Wheel wheel;
    private GasTank gasTank;

    public Car(int power, int maxSpeed, int energy, int consumption){

        dieselEngine = new DieselEngine(power, maxSpeed, consumption);
        wheel = new Wheel();
        gasTank = new GasTank(energy);
    }

    @Override
    public void accelerate() {
        wheel.accelerate(dieselEngine.getForce(gasTank.getEnergy(dieselEngine.getConsumption())), gasTank.getFuelVolume());
    }

    @Override
    public void stop() {
        wheel.stop();
        dieselEngine.stop();
    }

    @Override
    public void turn(String direction) {
        wheel.turn(direction);
    }
}
