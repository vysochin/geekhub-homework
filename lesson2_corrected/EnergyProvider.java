/**
 * Created by admin on 28.10.2014.
 */
public interface EnergyProvider {
    int getEnergy(int energy);
}
