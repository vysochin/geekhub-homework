import java.util.Scanner;

/**
 * Created by admin on 28.10.2014.
 */
public class TestDrive {
    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        Driveable ourVehicle;
        int command = 0;
        int choise = 1;
        int direction;

        System.out.print("Pleace chose your vehicle (1 Car, 2 Boat, 3 Solar-powered car) : ");
        choise = scanner.nextInt();

        if (choise == 1) {
            ourVehicle = new Car(7, 250, 200, 10);
            System.out.println("Car! good choice");
        } else {
            if (choise == 2) {
                ourVehicle = new Boat(5, 150, 70, 5);
                System.out.println("Boat! good choice");
            } else {
                ourVehicle = new SolarPoweredCar(5, 70, 50, 5);
                System.out.println("Solar-power car! good choice");
            }
        }

        System.out.println();

        while(true) {
            System.out.print("Please input command (1 accelerate, 2 turn, 3 stop, 0 exit) : ");
            command = scanner.nextInt();

            if (command == 1) {
                ourVehicle.accelerate();
                System.out.println();
            } else {
                if (command == 2) {
                    System.out.print("Please chose direction (1 right 2 left): ");
                    direction = scanner.nextInt();

                    if(direction == 1){
                        ourVehicle.turn("Right");
                    } else {
                        ourVehicle.turn("Left");
                    }

                    System.out.println();
                } else {
                    if (command == 3) {
                        ourVehicle.stop();
                        System.out.println();
                    } else {
                        System.out.print("Bye bye");
                        break;
                    }
                }
            }
        }
    }
}
