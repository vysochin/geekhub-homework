/**
 * Created by admin on 28.10.2014.
 */
public class Propeller implements ForceAcceptor {
    private int speed;

    @Override
    public void turn(String direction) {
        System.out.println("We turned to the "+direction);
    }

    @Override
    public void accelerate(int speed, int energy) {
        this.speed = speed;

        if(energy != 0) {
            System.out.println("We accelerated. Our speed is " + this.speed + "km/h");
        } else {
            System.out.println("Energy is Over :(");
        }
    }

    @Override
    public void stop() {
        speed = 0;
        System.out.println("We stoped...");
    }
}
