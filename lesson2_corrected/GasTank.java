/**
 * Created by admin on 28.10.2014.
 */
public class GasTank implements EnergyProvider {
    private int fuelVolume;

    public GasTank(int energy){
        fuelVolume = energy;
    }

    @Override
    public int getEnergy(int energy){
        if(fuelVolume - energy > 0){
            fuelVolume -= energy;
        } else {
            fuelVolume = 0;
        }

        return  energy;
    }

    int getFuelVolume(){
        if(fuelVolume >= 0) {
            return fuelVolume;
        } else {
            return 0;
        }
    }
}
