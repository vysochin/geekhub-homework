/**
 * Created by admin on 28.10.2014.
 */
public interface Driveable {
    public void accelerate();
    public void stop();
    public void turn(String direction);
}
