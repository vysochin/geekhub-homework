/**
 * Created by admin on 28.10.2014.
 */
public class SolarPoweredCar extends Vehicle{
    private ElectricEngine electricEngine;
    private Wheel wheel;
    private SolarBattery solarBattery;

    public SolarPoweredCar(int power, int maxSpeed, int energy, int consumption){

        electricEngine = new ElectricEngine(power, maxSpeed, consumption);
        wheel = new Wheel();
        solarBattery = new SolarBattery(energy);
    }

    @Override
    public void accelerate() {
        wheel.accelerate(electricEngine.getForce(solarBattery.getEnergy(electricEngine.getConsumption())), solarBattery.getElectrisityVolume());
    }

    @Override
    public void stop() {
        wheel.stop();
        electricEngine.stop();
    }

    @Override
    public void turn(String direction) {
        wheel.turn(direction);
    }
}
