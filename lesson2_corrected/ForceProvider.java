/**
 * Created by admin on 28.10.2014.
 */
public interface ForceProvider {
    int getForce(int force);
    void stop();
}
