/**
 * Created by admin on 28.10.2014.
 */
public class ElectricEngine implements ForceProvider {
    private int power;
    private int speed;
    private int maxSpeed;
    private int consumption;

    ElectricEngine(int power, int maxSpeed, int consumption){

        this.power = power;
        this.maxSpeed = maxSpeed;
        this.speed = 0;
        this.consumption = consumption;
    }

    @Override
    public int getForce(int energy) {
        speed += power * energy;

        if(speed < maxSpeed) {
            return speed;
        } else {
            return maxSpeed;
        }
    }

    @Override
    public void stop() {
        speed = 0;
    }

    int getConsumption(){
        return consumption;
    }
}
