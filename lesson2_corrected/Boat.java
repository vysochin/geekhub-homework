/**
 * Created by admin on 28.10.2014.
 */
public class Boat extends Vehicle{
    private DieselEngine dieselEngine;
    private Propeller propeller;
    private GasTank gasTank;

    public Boat(int power, int maxSpeed, int energy, int consumption){

        dieselEngine = new DieselEngine(power, maxSpeed, consumption);
        propeller = new Propeller();
        gasTank = new GasTank(energy);
    }

    @Override
    public void accelerate() {
        propeller.accelerate(dieselEngine.getForce(gasTank.getEnergy(dieselEngine.getConsumption())),gasTank.getFuelVolume());
    }

    @Override
    public void stop() {
        propeller.stop();
        dieselEngine.stop();
    }

    @Override
    public void turn(String direction) {
        propeller.turn(direction);
    }
}
