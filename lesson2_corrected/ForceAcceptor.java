/**
 * Created by admin on 28.10.2014.
 */
public interface ForceAcceptor {
    void turn(String direction);
    void accelerate(int speed, int energy);
    void stop();
}
