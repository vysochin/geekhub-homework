package source;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) throws IOException {
        URL url = new URL(pathToSource);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        return connection.getResponseCode() == 200;
    }

    @Override
    public String load(String pathToSource) throws IOException {

        String text = "";
        URL url = new URL(pathToSource);
        URLConnection urlConnection =  url.openConnection();
        InputStream is = urlConnection.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String inputLine;

        while ((inputLine = br.readLine()) != null){
            text += inputLine + "\n";
        }

        return text;
    }
}
