package source;

import java.io.*;
import java.util.Scanner;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File file = new File(pathToSource);
        if(file.exists()){
            return true;
        }
        return false;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        String sourceText = "";
        Scanner scanner = new Scanner(new File(pathToSource));

        while (scanner.hasNextLine()){
            sourceText +=  scanner.nextLine() + "\n";
        }
        return sourceText;
    }
}
