package org.geekhub.storage;

import org.geekhub.objects.Entity;
import org.geekhub.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link org.geekhub.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link org.geekhub.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try(Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        ResultSet resultSet = statement.executeQuery(sql);

        return extractResult(clazz, resultSet);
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        Statement statement = connection.createStatement();
        String sql = "DELETE FROM "+entity.getClass().getSimpleName()+" WHERE id = "+entity.getId();
        return statement.executeUpdate(sql) != 0;
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
        Statement statement = connection.createStatement();
        String sql;
        Set<String> keys = data.keySet();
        if (entity.isNew()) {
            String fields = "(";
            String values = "(";
            for (String key: keys){
                fields += key+", ";
                values += "\""+data.get(key)+"\""+", ";
            }
            fields = removeCharAt(fields, fields.length()-1) +")";
            fields = removeCharAt(fields, fields.length()-2);
            values = removeCharAt(values, values.length()-1) +")";
            values = removeCharAt(values, values.length()-2);
            sql = "INSERT INTO " + entity.getClass().getSimpleName() + " " + fields + " VALUES " + values;
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet key = statement.getGeneratedKeys();

            int lKey = 1;
            while (key.next()) {
                lKey = key.getInt(1);
            }
            entity.setId(lKey);
        } else {
            for (String key: keys){
                sql = "UPDATE " + entity.getClass().getSimpleName() + " SET " + key + " = " +"\""+ data.get(key) +"\""+ " WHERE id = " + entity.getId();
                statement.executeUpdate(sql);
            }
        }
    }



    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> map = new HashMap<>();
        Class clazz = entity.getClass();
        Field[] fields = clazz.getDeclaredFields();

        for(Field field: fields){
            field.setAccessible(true);
            if (field.getAnnotation(Ignore.class) != null) {
                continue;
            }
            if (field.getName().equals("admin")) {
                if (field.get(entity).equals(true)){
                    map.put(field.getName(), 1);
                } else {
                    map.put(field.getName(), 0);
                }
                continue;
            }
            map.put(field.getName(), field.get(entity));
            field.setAccessible(false);
        }

        return map;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {

        List<T> list = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();
        int i=0;
        while (resultSet.next()){
            list.add(clazz.newInstance());
            list.get(i).setId(resultSet.getInt("id"));
            for (Field field: fields){
                if (field.getAnnotation(Ignore.class) != null) {
                    continue;
                }
                field.setAccessible(true);
                field.set(list.get(i), resultSet.getObject(field.getName()));
                field.setAccessible(false);
            }
            i++;
        }

        return list;
    }

    public static String removeCharAt(String s, int pos) {
        return s.substring(0,pos)+s.substring(pos+1);
    }
}
