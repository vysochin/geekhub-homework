import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 15.11.2014.
 */
public interface TaskManager {
    public void addTask(Date data, Task task);
    public void removeTask(Task task);
    public Collection<String> getCategories();

    public Map<String, List<Task>> getTasksByCategories();
    public List<Task> getTasksByCategory(String category);
    public List<Task> getTasksForToday();
}
