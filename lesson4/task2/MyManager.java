import java.util.*;

/**
 * Created by admin on 15.11.2014.
 */
public class MyManager implements TaskManager{

    private Collection<String> categoryList = new HashSet<String>();
    private Map<String, List<Task>> mapCategoryTasks = new HashMap<String, List<Task>>();
    private Map<Date, List<Task>> mapDateTasks = new HashMap<Date, List<Task>>();

    @Override
    public void addTask(Date data, Task task) {
        List<Task> tmpTaskList1;
        List<Task> tmpTaskList2;

        if (mapCategoryTasks.containsKey(task.getCategory())) {
            tmpTaskList1 = mapCategoryTasks.get(task.getCategory());
        } else {
            tmpTaskList1 = new ArrayList<Task>();
        }

        if (mapDateTasks.containsKey(data)) {
            tmpTaskList2 =  mapDateTasks.get(data);
        } else {
            tmpTaskList2 = new ArrayList<Task>();
        }

        tmpTaskList1.add(task);
        tmpTaskList2.add(task);
        sort(tmpTaskList1);
        mapCategoryTasks.put(task.getCategory(), tmpTaskList1);
        mapDateTasks.put(data, tmpTaskList2);

        if (!(categoryList.contains(task.getCategory()))){
            categoryList.add(task.getCategory());
        }
    }

    @Override
    public void removeTask(Task task) {
        Collection<String> collection1 = mapCategoryTasks.keySet();
        Collection<Date> collection2 = mapDateTasks.keySet();

        for (String key: collection1) {
            List<Task> tmpTaskList =  mapCategoryTasks.get(key);

            if (tmpTaskList.contains(task)){
                tmpTaskList.remove(task);

                if (tmpTaskList.isEmpty()) {
                    categoryList.remove(key);
                    mapCategoryTasks.remove(key);
                } else {
                    mapCategoryTasks.put(key, tmpTaskList);
                }
                break;
            }
        }

        for (Date key: collection2) {
            List<Task> tmpTaskList =  mapDateTasks.get(key);

            if (tmpTaskList.contains(task)){
                tmpTaskList.remove(task);
                if (tmpTaskList.isEmpty()) {
                    mapDateTasks.remove(key);
                } else {
                    mapDateTasks.put(key, tmpTaskList);
                }
                break;
            }
        }
    }

    @Override
    public Collection<String> getCategories() {
        return categoryList;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        return mapCategoryTasks;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> taskList = mapCategoryTasks.get(category);
        if (categoryList.contains(category)) {
            taskList = sort(taskList);
        }
        return taskList;
    }

    @Override
    public List<Task> getTasksForToday() {
        List<Task> taskList = new ArrayList<Task>();
        Date date = new Date();

        Collection<Date> collection = mapDateTasks.keySet();

        for (Date key: collection) {
            if ((date.getTime()-86400000) < key.getTime()){
                taskList.addAll(mapDateTasks.get(key));
                break;
            }
        }

        if (!taskList.isEmpty()) {
            taskList = sort(taskList);
        }
        return taskList;
    }

    private <T extends Comparable> List<T> sort(List<T> list){
        Collections.sort(list);
        return list;
    }
}
