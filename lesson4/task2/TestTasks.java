import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 15.11.2014.
 */
public class TestTasks {
    public static void main(String args[]){
        Map<String, List<Task>> mapCategoryTasks;
        Task task1 = new Task("Category1", "Description 1");
        Task task2 = new Task("Category1", "Description 2");
        Task task3 = new Task("Category2", "Description 3");
        Task task4 = new Task("Category2", "Description 4");

        MyManager myManager = new MyManager();
        myManager.addTask(task1.setDate(new Date()), task1);
        myManager.addTask(task2.setDate(new Date()), task2);
        myManager.addTask(task3.setDate(new Date()), task3);
        myManager.addTask(task4.setDate(new Date()), task4);

        System.out.println("Categories   :       " + myManager.getCategories());

        mapCategoryTasks = myManager.getTasksByCategories();
        System.out.println("Tasks for today :    " + myManager.getTasksForToday());
        System.out.println("Map Category :       " + mapCategoryTasks);

        myManager.removeTask(task1);
        myManager.removeTask(task2);
        mapCategoryTasks = myManager.getTasksByCategories();
        System.out.println("Remove task1&task2 : " + mapCategoryTasks);

        System.out.println("Category1 :          " + myManager.getTasksByCategory("Category1"));
        System.out.println("Category2 :          " + myManager.getTasksByCategory("Category2"));

        System.out.println("Tasks for today :    " + myManager.getTasksForToday());
        System.out.println("Categories   :       " + myManager.getCategories());

    }
}
