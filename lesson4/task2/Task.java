import java.util.Date;

/**
 * Created by admin on 15.11.2014.
 */
public class Task implements Comparable<Task>{
    private String category;
    private String description;
    private Date date;

    Task(){}

    @Override
    public String toString() {
        return description;
    }

    Task(String category, String description){
        this.category = category;
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public int compareTo(Task o) {
        if (this.date.getTime() > o.getDate()) {
            return 1;
        } else {
            if (this.date.getTime() < o.getDate()) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    public long getDate(){
        return date.getTime();
    }

    public Date setDate(Date date){
        this.date = date;
        return date;
    }
}
