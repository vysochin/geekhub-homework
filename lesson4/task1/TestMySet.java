import java.util.HashSet;
import java.util.Set;

/**
 * Created by admin on 15.11.2014.
 */
public class TestMySet {
    public static void main(String args[]){
        SetOperator setOperator = new SetOperator();
        Set set1 = new HashSet<Integer>();
        Set set2 = new HashSet<Integer>();
        Set set3;

        for (int i = 0; i < 10; i++){
            set1.add(i);
        }

        for (int i = 5; i < 15; i++){
            set2.add(i);
        }

        System.out.println("Set1 : " + set1);
        System.out.println("Set2 : " + set2);

        System.out.println("Equals : " + setOperator.equals(set1, set2));
        System.out.println("Equals : " + setOperator.equals(set1, set1));

        set3 = setOperator.union(set1, set2);
        System.out.println("Union :              " + set3);

        set3 = setOperator.subtract(set1, set2);
        System.out.println("Subtract :           " + set3);

        set3 = setOperator.intersect(set1, set2);
        System.out.println("Intersect :          " + set3);

        set3 = setOperator.symmetricSubtract(set1, set2);
        System.out.println("Symmetric Subtract : " + set3);
    }
}
