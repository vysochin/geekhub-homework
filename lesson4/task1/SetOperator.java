import java.util.HashSet;
import java.util.Set;

/**
 * Created by admin on 15.11.2014.
 */
public class SetOperator implements SetOperations{

    SetOperator(){}

    @Override
    public boolean equals(Set a, Set b) {
        return a.equals(b);
    }

    @Override
    public Set union(Set a, Set b) {
        Set tmpSet = new HashSet<Integer>();
        tmpSet.addAll(a);
        tmpSet.addAll(b);
        return tmpSet;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set tmpSet = new HashSet<Integer>();

        for (Object i :a){
            if (!(b.contains(i))){
                tmpSet.add(i);
            }
        }
        return tmpSet;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set tmpSet = new HashSet<Integer>();

        for (Object i :a){
            if (b.contains(i)){
                tmpSet.add(i);
            }
        }
        return tmpSet;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set tmpSet = new HashSet<Integer>();

        for (Object i :a){
            if (!(b.contains(i))){
                tmpSet.add(i);
            }
        }

        for (Object i :b){
            if (!(a.contains(i))){
                tmpSet.add(i);
            }
        }
        return tmpSet;
    }
}
