package org.geekhub;

import sun.net.www.URLConnection;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getData(URL url) throws IOException {
        //implement me
        java.net.URLConnection uc = url.openConnection();
        DataInputStream is = new DataInputStream(uc.getInputStream());
        byte[] b = new byte[is.available()*2];
        int length;
        while((length = is.read(b)) > 0) {}
        is.close();
        return b;
    }
}
