/**
 * Created by Denys Vysochin on 03.11.2014.
 */
public class StringTest {
    public static void main (String args[]) {
        String stringLine1 = "Denys";
        String stringLine2 = " Vysochin";
        StringBuffer stringBufferLine1 = new StringBuffer("Denys");
        StringBuffer stringBufferLine2 = new StringBuffer(" Vysochin");
        StringBuilder stringBuilderLine1 = new StringBuilder("Denys");
        StringBuilder stringBuilderLine2 = new StringBuilder(" Vysochin");
        long time = 0;
        int nIteration = 10000;

        time = System.currentTimeMillis();

        for (int i = 0; i < nIteration; i++) {
            stringBufferLine1.append(stringBufferLine2);
        }

        System.out.println("StringBuffer  time = "+ (System.currentTimeMillis()-time));

        time = System.currentTimeMillis();

        for (int i = 0; i < nIteration; i++) {
            stringBuilderLine1.append(stringBuilderLine2);
        }

        System.out.println("StringBuilder time = "+ (System.currentTimeMillis()-time));

        time = System.currentTimeMillis();

        for (int i = 0; i < nIteration; i++) {
            stringLine1 += stringLine2;
        }

        System.out.println("String time        = "+ (System.currentTimeMillis()-time));
    }
}
