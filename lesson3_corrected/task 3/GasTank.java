/**
 * Created by admin on 28.10.2014.
 */
public class GasTank implements EnergyProvider {
    private int fuelVolume;
    private int maxFuelVolume;

    public GasTank(int maxFuelVolume){
        fuelVolume = maxFuelVolume;
        this.maxFuelVolume = maxFuelVolume;
    }

    @Override
    public void giveFuel(int fuel) throws FuelOverflowException{
        if (fuelVolume + fuel < maxFuelVolume) {
            fuelVolume += fuel;
        } else {
            fuelVolume = maxFuelVolume;
            throw new FuelOverflowException("Gas Tank is overflow");
        }
    }

    @Override
    public int getEnergy(int energy) throws FuelOverException{
        if(fuelVolume - energy > 0){
            fuelVolume -= energy;
        } else {
            fuelVolume = 0;
            throw new FuelOverException("Gas Tank is empty.");
        }
        return  energy;
    }

    @Override
    public void showEnergy() {
        System.out.println("Fuel = " + fuelVolume);
    }

}
