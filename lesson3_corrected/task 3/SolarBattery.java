/**
 * Created by admin on 28.10.2014.
 */
 public class SolarBattery implements EnergyProvider {
    private int electrisityVolume;
    private int maxFuelVolume;

    public SolarBattery(int maxFuelVolume){
        electrisityVolume = maxFuelVolume;
        this.maxFuelVolume = maxFuelVolume;
    }

    @Override
    public void giveFuel(int fuel) throws FuelOverflowException{
        if (electrisityVolume + fuel < maxFuelVolume) {
            electrisityVolume += fuel;
        } else {
            electrisityVolume = maxFuelVolume;
            throw new FuelOverflowException("Gas Tank is overflow");
        }
    }

    @Override
    public int getEnergy(int energy) throws FuelOverException{

        if(electrisityVolume - energy > 0){
            electrisityVolume -= energy;
        } else {
            electrisityVolume = 0;
            throw new FuelOverException("Solar battery is empty");
        }

        return  energy;
    }

    @Override
    public void showEnergy() {
        System.out.println("Fuel = " + electrisityVolume);
    }

    int getElectrisityVolume(){
        return electrisityVolume;
    }
}
