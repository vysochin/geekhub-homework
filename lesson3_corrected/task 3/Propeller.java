/**
 * Created by admin on 28.10.2014.
 */
public class Propeller implements ForceAcceptor {

    @Override
    public void turn(String direction) {
        System.out.println("We turned to the " + direction);
    }

    @Override
    public void stop() {
        System.out.println("Propeller not spin");
    }

    @Override
    public void go(int force) {
        System.out.println("Propeller spin");
    }
}
