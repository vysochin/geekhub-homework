/**
 * Created by admin on 28.10.2014.
 */
public class Boat extends Vehicle{
    private DieselEngine dieselEngine;
    private Propeller propeller;
    private GasTank gasTank;

    public Boat(int maxSpeed, int consumption){

        dieselEngine = new DieselEngine(consumption);
        propeller = new Propeller();
        gasTank = new GasTank(200);
        this.maxSpeed = maxSpeed;
    }

    @Override
    public void accelerate(int speed) throws SpeedLimitException {
        if (this.speed + speed < maxSpeed) {
            this.speed += speed;
        } else {
            this.speed = maxSpeed;
            throw new SpeedLimitException("Reached a Maximum speed");
        }
        System.out.println("We Accelerated...");
    }

    public void goTen() throws FuelOverException {
        propeller.go(dieselEngine.getForce(gasTank.getEnergy(dieselEngine.getConsumption()/10)));
    }

    @Override
    public void showEnergy() {
        gasTank.showEnergy();
    }

    @Override
    public void speedDown() {
        if (this.speed - speed >= 0) {
            this.speed -= speed;
        } else {
            this.stop();
        }
    }

    @Override
    public void stop() {
        propeller.stop();
    }

    @Override
    public void fill() throws FuelOverflowException {
        gasTank.giveFuel(10);
    }

    @Override
    public void showSpeed() {
        System.out.println("Our speed = " + this.speed + " km/h");
    }

    @Override
    public void turn(String direction) {
        propeller.turn(direction);
    }

    @Override
    public void driveHundredKilometers() throws FuelOverException, SpeedLimitException {
        for (int i = 0; i < 10; i++){
            propeller.go(dieselEngine.getForce(gasTank.getEnergy(dieselEngine.getConsumption()/10)));
        }
    }
}
