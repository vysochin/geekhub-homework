/**
 * Created by admin on 28.10.2014.
 */
public class Wheel implements ForceAcceptor {

    @Override
    public void turn(String direction) {
        System.out.println("We turned to the " + direction);
    }

    @Override
    public void stop() {
        System.out.println("Wheel not spin");
    }

    @Override
    public void go(int force) {
        System.out.println("Wheel spin");
    }
}