/**
 * Created by admin on 28.10.2014.
 */
public interface ForceAcceptor {
    public void turn(String direction);
    public void stop();
    public void go(int force);
}