/**
 * Created by admin on 28.10.2014.
 */
public class ElectricEngine implements ForceProvider {
    private int force;
    private int consumption;

    ElectricEngine(int consumption){
        this.consumption = consumption;
    }

    @Override
    public int getForce(int energy){
        force = energy;
        return force;
    }

    int getConsumption(){
        return consumption;
    }
}
