/**
 * Created by admin on 28.10.2014.
 */
public interface EnergyProvider {
    public void giveFuel(int fuel) throws FuelOverflowException;
    public int  getEnergy(int energy) throws FuelOverException;
    public void showEnergy();
}
