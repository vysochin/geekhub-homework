/**
 * Created by admin on 28.10.2014.
 */
public class SolarPoweredCar extends Vehicle{
    private ElectricEngine electricEngine;
    private Wheel wheel;
    private SolarBattery solarBattery;

    public SolarPoweredCar(int maxSpeed, int consumption){

        electricEngine = new ElectricEngine(consumption);
        wheel = new Wheel();
        solarBattery = new SolarBattery(250);
        this.maxSpeed = maxSpeed;
    }

    @Override
    public void accelerate(int speed) throws SpeedLimitException {
        if (this.speed + speed < maxSpeed) {
            this.speed += speed;
        } else {
            this.speed = maxSpeed;
            throw new SpeedLimitException("Reached a Maximum speed");
        }
        System.out.println("We Accelerated...");
    }

    public void goTen() throws FuelOverException {
        wheel.go(solarBattery.getEnergy(electricEngine.getForce(electricEngine.getConsumption()/10)));
    }

    @Override
    public void showEnergy() {
        solarBattery.showEnergy();
    }

    @Override
    public void speedDown() {
        if (this.speed - speed >= 0) {
            this.speed -= speed;
        } else {
            this.stop();
        }
    }

    @Override
    public void stop() {
        wheel.stop();
    }

    @Override
    public void fill() throws FuelOverflowException {
        solarBattery.giveFuel(10);
    }

    @Override
    public void showSpeed() {
        System.out.println("Our speed = " + this.speed + " km/h");
    }

    @Override
    public void turn(String direction) {
        wheel.turn(direction);
    }

    @Override
    public void driveHundredKilometers() throws FuelOverException, SpeedLimitException {
        for (int i = 0; i < 10; i++){
            wheel.go(solarBattery.getEnergy(electricEngine.getForce(electricEngine.getConsumption()/10)));
        }
    }
}
