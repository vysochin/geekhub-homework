/**
 * Created by admin on 03.11.2014.
 */
public class SpeedLimitException extends Exception {
    public SpeedLimitException(){}
    public SpeedLimitException(String msg){
        super(msg);
    }
}
