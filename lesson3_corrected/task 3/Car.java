/**
 * Created by admin on 28.10.2014.
 */
public class Car extends Vehicle {
    private DieselEngine dieselEngine;
    private Wheel wheel;
    private GasTank gasTank;

    public Car(int maxSpeed, int consumption){

        dieselEngine = new DieselEngine(consumption);
        wheel = new Wheel();
        gasTank = new GasTank(300);
        this.maxSpeed = maxSpeed;
    }

    @Override
    public void accelerate(int speed) throws SpeedLimitException {
        if (this.speed + speed < maxSpeed) {
            this.speed += speed;
        } else {
            this.speed = maxSpeed;
            throw new SpeedLimitException("Reached a Maximum speed");
        }
        System.out.println("We Accelerated...");
    }

    public void goTen() throws FuelOverException {
        wheel.go(dieselEngine.getForce(gasTank.getEnergy(dieselEngine.getConsumption()/10)));
    }

    @Override
    public void showEnergy() {
        gasTank.showEnergy();
    }

    @Override
    public void speedDown() {
        if (this.speed - speed >= 0) {
            this.speed -= speed;
        } else {
            this.stop();
        }
    }

    @Override
    public void stop() {
        wheel.stop();
    }

    @Override
    public void fill() throws FuelOverflowException {
        gasTank.giveFuel(10);
    }

    @Override
    public void showSpeed() {
        System.out.println("Our speed = " + this.speed + " km/h");
    }

    @Override
    public void turn(String direction) {
        wheel.turn(direction);
    }

    @Override
    public void driveHundredKilometers() throws FuelOverException, SpeedLimitException {
        for (int i = 0; i < 10; i++){
            wheel.go(dieselEngine.getForce(gasTank.getEnergy(dieselEngine.getConsumption()/10)));
        }
    }

}
