/**
 * Created by admin on 04.11.2014.
 */
public class FuelOverflowException extends Exception {
    public FuelOverflowException(){}
    public FuelOverflowException(String msg){
        super(msg);
    }
}
