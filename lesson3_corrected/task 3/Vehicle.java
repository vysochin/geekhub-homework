/**
 * Created by admin on 28.10.2014.
 */
abstract public class Vehicle implements Driveable{
    protected int speed;
    protected int maxSpeed;
    public void turn(String direction){}
    public void accelerate() throws SpeedLimitException {}
    public void stop(){}
    public void speedDown(){}
    public void driveHundredKilometers() throws FuelOverException, SpeedLimitException {}
    public void fill() throws FuelOverflowException {}

}
