/**
 * Created by admin on 28.10.2014.
 */
public interface Driveable {
    public void accelerate(int speed) throws SpeedLimitException;
    public void speedDown();
    public void stop();
    public void turn(String direction);
    public void driveHundredKilometers() throws FuelOverException, SpeedLimitException;
    public void fill() throws FuelOverflowException;
    public void showSpeed();
    public void goTen() throws FuelOverException;
    public void showEnergy();
}
