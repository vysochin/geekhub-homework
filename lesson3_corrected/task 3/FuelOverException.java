/**
 * Created by admin on 03.11.2014.
 */
public class FuelOverException extends Exception {
    public FuelOverException(){}
    public FuelOverException(String msg){
        super(msg);
    }
}
