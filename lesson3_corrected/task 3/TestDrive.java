import java.util.Scanner;

/**
 * Created by Denys Vysochin on 28.10.2014.
 */
public class TestDrive {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        Vehicle ourVehicle;
        String  command = "";
        int     choice = 1;

        System.out.print("Please chose your vehicle (1 Car, 2 Boat, 3 Solar-powered car) : ");
        choice = scanner.nextInt();

        if (choice == 1) {
            ourVehicle = new Car(250, 10);
            System.out.println("Car! good choice");
        } else {
            if (choice == 2) {
                ourVehicle = new Boat(150, 10);
                System.out.println("Boat! good choice");
            } else {
                ourVehicle = new SolarPoweredCar(70, 10);
                System.out.println("Solar-power car! good choice");
            }
        }

        System.out.println();

        while (true) {
            System.out.print("Please input next command : ");
            command = scanner.nextLine();

            if (command.equals("?")) {
                System.out.println("'a'     Accelerate");
                System.out.println("'g'     Go");
                System.out.println("'f'     Fill Fuel");
                System.out.println("'d'     Down speed");
                System.out.println("'s'     Stop");
                System.out.println("'tr'    Turn Right");
                System.out.println("'tl'    Turn Left");
                System.out.println("'100'   Drive hundred Kilometers");
                System.out.println("'speed' Show speed");
                System.out.println("'fuel'  Show fuel");
                System.out.println("'?'     Help");
                } else {
                if (command.equals("a")) {
                    try {
                        ourVehicle.accelerate(10);
                    } catch (SpeedLimitException e1) {
                        System.out.println(e1);
                    }
                } else {
                    if (command.equals("f")) {
                        try {
                            ourVehicle.fill();
                        } catch (FuelOverflowException e2) {
                            System.out.println(e2);
                        }
                    } else {
                        if (command.equals("d")) {
                            ourVehicle.speedDown();
                        } else {
                            if (command.equals("s")) {
                                ourVehicle.stop();
                            } else {
                                if (command.equals("100")) {
                                    try {
                                        ourVehicle.driveHundredKilometers();
                                    } catch (SpeedLimitException e3) {
                                        System.out.println(e3);
                                    } catch (FuelOverException e4) {
                                        System.out.println(e4);
                                    }
                                } else {
                                    if (command.equals("g")) {
                                        try {
                                            ourVehicle.goTen();
                                        } catch (FuelOverException e5) {
                                            System.out.println(e5);
                                        }
                                    } else {
                                        if (command.equals("speed")) {
                                            ourVehicle.showSpeed();
                                        } else {
                                            if (command.equals("fuel")) {
                                                ourVehicle.showEnergy();
                                            } else {
                                                if (command.equals("tr")){
                                                    ourVehicle.turn("right");
                                                } else {
                                                    if (command.equals("tl")){
                                                        ourVehicle.turn("left");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
