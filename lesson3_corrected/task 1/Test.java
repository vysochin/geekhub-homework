import com.sun.org.apache.xml.internal.serializer.utils.SerializerMessages_sv;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Denys Vysochin
 */
public class Test {

    public static void main(String args[]){
        ArrayList<Cat> catList = new ArrayList<Cat>();

        catList.add(0, new Cat(5));
        catList.add(1, new Cat(4));
        catList.add(2, new Cat(1));
        catList.add(3, new Cat(3));
        catList.add(4, new Cat(2));

        System.out.println("Cat List : ");
        for (int i = 0; i < catList.size(); i++){
            System.out.println("Cat  "+ catList.get(i).getAge() + " age");
        }

        System.out.println();
        ArrayList<Cat> tmpCatList = Cat.sort(catList);
        System.out.println("Sort Cat :3 ");
        System.out.println();

        System.out.println("Cat List : ");
        for (int i = 0; i < catList.size(); i++){
            System.out.println("Cat  "+ catList.get(i).getAge() + " age");
        }

        System.out.println();
        System.out.println("tmp Cat List : ");
        for (int i = 0; i < tmpCatList.size(); i++){
            System.out.println("Cat  "+ tmpCatList.get(i).getAge() + " age");
        }
    }
}
