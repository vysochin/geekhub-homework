import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

public class Cat implements Comparable<Cat> {

    private int age;

    Cat() {}

    Cat(int age) {
        this.age = age;
    }

    int getAge() {
        return age;
    }

    @Override
    public int compareTo(Cat cat) {

        if (this.age > cat.getAge()) {
            return 1;
        } else {
            if (this.age < cat.getAge()) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    public static <T extends Comparable> ArrayList<T> sort(ArrayList<T> catList) {
        ArrayList<T> tmpCatList = (ArrayList<T>) catList.clone();
        Collections.sort(tmpCatList);
        return tmpCatList;
    }
}
