/**
 * Created by admin on 29.01.2015.
 */
public class Animals {
    private String kind;
    private String name;
    private int id;

    public String getKind() {
        return kind;
    }

    public String getName() {
        return name;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public void setName(String name) {
        this.name = name;
    }

}
