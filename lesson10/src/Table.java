/**
 * Created by admin on 29.01.2015.
 */
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

@WebServlet(name="tableServlet", urlPatterns = {"/table"})
public class Table extends HttpServlet {

    public ListController listController = new ListController();
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if(!"0".equals(req.getParameter("delete"))){
            listController.deleteAnimals(req.getParameter("kind"), req.getParameter("name"));
        }
        req.setAttribute("listOfAnimal", listController.getArrayOfAnimals());
        req.setAttribute("listOfKind", listController.getArrayOfKind());
        req.getRequestDispatcher("table.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        listController.addAnimals(req.getParameter("kind"), req.getParameter("name"));
        req.setAttribute("listOfAnimal", listController.getArrayOfAnimals());
        req.setAttribute("listOfKind", listController.getArrayOfKind());
        req.getRequestDispatcher("table.jsp").forward(req,resp);
    }
}
