import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 29.01.2015.
 */
public class ListController {

    DatabaseStorage databaseStorage = new DatabaseStorage();
    List<Animals> listOfAnimals = new ArrayList<>();
    List<String> listOfKind = new ArrayList<>();

    private  Animals getAnimals(String kind, String name){

        updateData();
        for (Animals animal: listOfAnimals){
            if (kind.equals(animal.getKind()) && name.equals(animal.getName())){
                return animal;
            }
        }
        return null;
    }

    public void addAnimals(String kind, String name){

        try {
            databaseStorage.addAnimal(kind, name);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        updateData();
    }

    public void deleteAnimals(String kind, String name){

        try {
            databaseStorage.deleteAnimal(this.getAnimals(kind,name).getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        updateData();
    }

    public String[] getArrayOfKind(){

        updateData();
        String[] arrayOfKind = new String[listOfKind.size()];
        for(int i=0; i<listOfKind.size(); i++){
            arrayOfKind[i] = listOfKind.get(i);
        }
        return arrayOfKind;
    }

    public Animals[] getArrayOfAnimals(){

        updateData();
        Animals[] arrayOfAnimals = new Animals[listOfAnimals.size()];

        for(int i=0; i<listOfAnimals.size(); i++){
            arrayOfAnimals[i] = listOfAnimals.get(i);
        }
        return arrayOfAnimals;
    }
    private void updateData(){

        try {
            listOfAnimals = databaseStorage.getListOfAnimals();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(Animals animal:listOfAnimals){
            if(!listOfKind.contains(animal.getKind())){
                listOfKind.add(animal.getKind());
            }
        }
    }
}
