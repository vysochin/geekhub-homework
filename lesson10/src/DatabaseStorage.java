

import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;


/**
 * Created by admin on 01.02.2015.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/AnimalsTable", "root", "root");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Animals> getListOfAnimals() throws SQLException {

        List<Animals> listOfAnimals = new ArrayList<>();
        Statement statement= connection.createStatement();
        String sql= "SELECT * FROM animals";
        ResultSet result= statement.executeQuery(sql);

        while(result.next()) {
            Animals animals = new Animals();
            animals.setId(result.getInt("id"));
            animals.setKind(result.getString("kind"));
            animals.setName(result.getString("name"));
            listOfAnimals.add(animals);
        }
        return listOfAnimals;
    }

    @Override
    public void deleteAnimal(int id) throws SQLException {

        Statement statement = connection.createStatement();
        String sql = "DELETE FROM animals WHERE id = "+id;
        statement.executeUpdate(sql);
    }

    @Override
    public void addAnimal(String kind, String name) throws SQLException {

        Statement statement = connection.createStatement();
        String sql = "INSERT INTO animals (kind, name) VALUES ("+"\""+kind+"\""+", \""+name+"\")";
        statement.executeUpdate(sql);
    }
}
