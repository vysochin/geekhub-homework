import java.sql.SQLException;
import java.util.List;

/**
 * Created by admin on 01.02.2015.
 */
public interface Storage {
    public List getListOfAnimals() throws SQLException;
    public void deleteAnimal(int id) throws SQLException;
    public void addAnimal(String kind, String name) throws SQLException;

}
