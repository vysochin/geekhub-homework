<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 30.01.2015
  Time: 14:47
  To change this template use File | Settings | File Templates.
--%>
<%--<%!ListController listController = new ListController();%>--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Table</title>
</head>
<body>
<form action="http://localhost:8080/table" method="post">
    <table border="1" width="500" cellspacing="0">
        <tr align="center">
            <th><b>Name</b></th>
            <th><b>Value</b></th>
            <th><b>Action</b></th>
        </tr>
        <c:forEach var="kind" items="${listOfKind}">
            <c:set var="tmp" scope="session" value="${1}"/>
            <c:forEach var="animal" items="${listOfAnimal}">
                <tr><c:if test="${animal.kind eq kind}">
                    <c:choose>
                        <c:when test="${tmp eq 1}">
                            <th>${animal.kind}</th>
                            <c:set var="tmp" scope="session" value="${0}"/>
                        </c:when>
                        <c:when test="${tmp eq 0}">
                            <th></th>
                        </c:when>
                    </c:choose>
                    <th>${animal.name}</th>
                    <th>
                        <a href="http://localhost:8080/table?delete=1&kind=${animal.kind}&name=${animal.name}">delete</a>
                    </th>
                </c:if>
                </tr>
            </c:forEach>
        </c:forEach>
        <tr align="center">
            <th><input type="text" name="kind"></th>
            <th><input type="text" name="name"></th>
            <th><input type="submit" name="send" value="add"></th>
        </tr>
    </table>

</form>
</body>
</html>
