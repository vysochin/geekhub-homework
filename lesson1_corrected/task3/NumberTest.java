import java.util.Scanner;


public class NumberTest {
    public static void main(String args[]){
        Number number = new Number();
        Scanner scanner = new Scanner(System.in);
        String s;
        Integer n;

        while (true) {
            System.out.print("Input next number 0..9 (q Exit): ");
            s=scanner.nextLine();

            if (s.equals("q")) {
                break;
            }
            n = Integer.parseInt(s);
            System.out.print("Method 1: "+n+" = ");
            number.printNumber1(n);
            System.out.print("Method 2: "+n+" = ");
            number.printNumber2(n);
            System.out.print("Method 3: "+n+" = ");
            number.printNumber3(n);
            System.out.print("Method 4: "+n+" = ");
            number.printNumber4(n);
        }
    }
}
