
public class Number {


    public void printNumber1(int n) {
        if (n == 0)
            System.out.println("Zero");
        else if (n == 1)
            System.out.println("One");
        else if (n == 2)
            System.out.println("Two");
        else if (n == 3)
            System.out.println("Three");
        else if (n == 4)
            System.out.println("Four");
        else if (n == 5)
            System.out.println("Five");
        else if (n == 6)
            System.out.println("Six");
        else if (n == 7)
            System.out.println("Seven");
        else if (n == 8)
            System.out.println("Eight");
        else if (n == 9)
            System.out.println("Nine");
    }


    public void printNumber2(int n){

        switch (n){
            case 0:
                System.out.println("Zero");
                    break;
            case 1:
                System.out.println("One");
                    break;
            case 2:
                System.out.println("Two");
                    break;
            case 3:
                System.out.println("Three");
                    break;
            case 4:
                System.out.println("Four");
                    break;
            case 5:
                System.out.println("Five");
                    break;
            case 6:
                System.out.println("Six");
                    break;
            case 7:
                System.out.println("Seven");
                    break;
            case 8:
                System.out.println("Eight");
                    break;
            case 9:
                System.out.println("Nine");
                    break;
        }
    }

    public void printNumber3(int n){
        String s[] = new String[10];
        s[0] = "Zero";
        s[1] = "One";
        s[2] = "Two";
        s[3] = "Three";
        s[4] = "Four";
        s[5] = "Five";
        s[6] = "Six";
        s[7] = "Seven";
        s[8] = "Eight";
        s[9] = "Nine";
        System.out.println(s[n]);
    }

    public void printNumber4(int n) {
        String s = "";
            s = (n==0) ? "Zero": (n==1)
                    ? "One"
                    : (n==2)
                    ? "Two"
                    : (n==3)
                    ? "Three"
                    : (n==4)
                    ? "Four"
                    : (n==5)
                    ? "Five"
                    : (n==6)
                    ? "Six"
                    : (n==7)
                    ? "Seven"
                    : (n==8)
                    ? "Eight"
                    : "Nine";
            System.out.println(s);
    }
}
