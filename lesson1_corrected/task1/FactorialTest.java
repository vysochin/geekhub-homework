import java.util.Scanner;

/**
 * Created by Denys Vysochin on 25.10.2014.
 */
public class FactorialTest {

    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        MyFactorial factorial = new MyFactorial();
        int n;

        System.out.print("Input N : ");
        n = scanner.nextInt();

        System.out.println("MyFactorial  of " + n + " = " + factorial.getFactorial(n));
    }

}
