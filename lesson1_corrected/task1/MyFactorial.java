

public class MyFactorial {
    public long getFactorial(int n) {
        long fact=1;
        for (int i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }
}
