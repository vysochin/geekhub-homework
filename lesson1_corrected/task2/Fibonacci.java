import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 16.10.14
 * Time: 19:07
 * To change this template use File | Settings | File Templates.
 */
public class Fibonacci {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n;
        int fibonacci1 = 1;
        int fibonacci2 = 1;

        System.out.print("Input N (N > 0) : ");
        n = scanner.nextInt();
        System.out.print("Fibonacci array : " + fibonacci1);

        for (int i = 2; i <= n; i++) {
            int tmp;
            System.out.print(" " + fibonacci2);
            tmp = fibonacci2;
            fibonacci2 += fibonacci1;
            fibonacci1 = tmp;
        }
    }
}
