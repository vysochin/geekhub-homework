package geekhub;

import geekhub.source.SourceLoader;
import geekhub.source.SourceProvider;
import geekhub.source.URLSourceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Scanner;

@Component
public class TranslatorController {

    //@Autowired private SourceLoader sourceLoader ;
    //@Autowired private Translator translator;

    public static void main(String[] args) throws IOException {
        //initialization
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        SourceLoader sourceLoader = context.getBean(SourceLoader.class);
        Translator translator = context.getBean(Translator.class);

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while(!"exit".equals(command)) {
            //      So, the only way to stop the application is to do that manually or type "exit"
            try {
                String source = sourceLoader.loadSource(command);
                System.out.println("Original: " + source);
                String translation = translator.translate(source);
                System.out.println("Translation: " + translation);
            } catch (IOException e){
                System.out.println("Please input correct path");
            }
            command = scanner.next();
        }
    }

    /*public void method(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while(!"exit".equals(command)) {
            //      So, the only way to stop the application is to do that manually or type "exit"
            try {
                String source = sourceLoader.loadSource(command);
                System.out.println("Original: " + source);
                String translation = translator.translate(source);
                System.out.println("Translation: " + translation);
            } catch (IOException e){
                System.out.println("Please input correct path");
            }
            command = scanner.next();
        }
    }*/
}
