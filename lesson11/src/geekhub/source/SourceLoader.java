package geekhub.source;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */

@Component
public class SourceLoader {

    @Autowired private FileSourceProvider fileSourceProvider;
    @Autowired private URLSourceProvider urlSourceProvider;

    public String loadSource(String pathToSource) throws IOException {

        if (fileSourceProvider.isAllowed(pathToSource)){
            return fileSourceProvider.load(pathToSource);
        } else if (urlSourceProvider.isAllowed(pathToSource)){
            return urlSourceProvider.load(pathToSource);
        } else {
            return null;
        }
    }
}