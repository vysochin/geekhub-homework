<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<body>
<h1>Hibernate Example</h1>

<h2>Users</h2>

<form action="${pageContext.request.contextPath}/users" method="post">
    <c:if test="${groups}">
        <input name="firstName" placeholder="First Name">
        <input name="lastName" placeholder="Last Name">
        <input name="email" type="email" placeholder="Email">
        <select name="group">
            <!--<option>New Group</option>-->
            <c:forEach items="${groups}" var="group">
                <option>${group.name}</option>
            </c:forEach>
        </select>
        <input type="submit" value="Submit">
    </c:if>
</form>

<table border="1" cellspacing="0">
    <tr>
        <td>ID</td>
        <td>FIRST NAME</td>
        <td>LAST NAME</td>
        <td>EMAIL</td>
        <td>GROUP</td>
        <td>ACTIONS</td>
    </tr>

    <c:forEach var="user" items="${users}">
        <form action="${pageContext.request.contextPath}/save?id=${user.id}" method="post">
            <tr>
                <td>${user.id}</td>
                <td><input name="firstName" value="${user.firstName}"></td>
                <td><input name="lastName" value="${user.lastName}"></td>
                <td><input name="email" value="${user.email}"></td>
                <td><input name="group" value="${user.group.name}"></td>
                <td><input type="submit" value="Save">
                    <a href="${pageContext.request.contextPath}/delete?id=${user.id}">Delete</a></td>

            </tr>
        </form>
    </c:forEach>
</table>
<h2>Groups</h2>

<form action="${pageContext.request.contextPath}/addgroup" method="post">
    <input name="name" placeholder="Group Name">
    <input type="submit" value="Submit">
</form>

<table border="1" cellspacing="0">
    <tr>
        <td>ID</td>
        <td>NAME</td>
        <td>ACTIONS</td>
    </tr>
    <c:forEach items="${groups}" var="group">
        <form action="${pageContext.request.contextPath}/savegroup?id=${group.id}" method="post">
            <tr>
                <td>${group.id}</td>
                <td><input name="name" value="${group.name}"></td>
                <td><input type="submit" value="Save">
                    <a href="${pageContext.request.contextPath}/deletegroup?id=${group.id}">Delete</a></td>
            </tr>
        </form>
    </c:forEach>
</table>
</body>
</html>