package ua.ck.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ck.geekhub.entity.User;
import ua.ck.geekhub.entity.Group;

@Controller
public class MainController {

    @Autowired
    UserService userService;
    @Autowired
    GroupService groupService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        return "redirect:users";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String user(ModelMap model) {
        model.addAttribute("users", userService.getUsers());
        model.addAttribute("groups", groupService.getGroups());
        return "hello";
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String createUser(
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String email,
            @RequestParam String group
    ) {
        userService.createUser(firstName, lastName, email, group);
        return "redirect:users";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteUser(@RequestParam(value = "id", required = true) Integer id,
                             ModelMap model) {
        userService.deleteUser(userService.getUser(id));
        return "redirect:users";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveUser(@RequestParam Integer id,
                           @RequestParam String firstName,
                           @RequestParam String lastName,
                           @RequestParam String email,
                           @RequestParam String group,
                           ModelMap model) {
        User user = userService.getUser(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setGroup(groupService.getGroup(group));
        userService.saveUser(user);
        return "redirect:users";
    }

    @RequestMapping(value = "/savegroup", method = RequestMethod.POST)
    public String saveGroup(@RequestParam String name,
                            @RequestParam Integer id) {
        Group group = groupService.getGroup(id);
        group.setName(name);
        groupService.saveGroup(group);
        return "redirect:users";
    }

    @RequestMapping(value = "/deletegroup", method = RequestMethod.GET)
    public String deleteGroup(@RequestParam Integer id) {
        groupService.deleteGroup(groupService.getGroup(id));
        return "redirect:users";
    }

    @RequestMapping(value = "/addgroup", method = RequestMethod.POST)
    public String addGroup(@RequestParam String name) {
        groupService.createGroup(name);
        return "redirect:users";
    }
}