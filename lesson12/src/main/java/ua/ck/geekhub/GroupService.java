package ua.ck.geekhub;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.entity.Group;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by admin on 16.02.2015.
 */
@Repository
@Transactional
public class GroupService {

    @Autowired
    SessionFactory sessionFactory;

    public void saveGroup(Group group) {
        sessionFactory.getCurrentSession().saveOrUpdate(group);
    }

    public Group getGroup(Integer id) {
        return (Group) sessionFactory.getCurrentSession().get(Group.class, id);
    }

    public void deleteGroup(Group group) {
        sessionFactory.getCurrentSession().delete(group);
    }

    public Group getGroup(String name) {
        if(!isPresent(name)){
            createGroup(name);
        }
        return (Group) sessionFactory.getCurrentSession().get(Group.class, getIdByName(name));
    }

    public List<Group> getGroups() {
        return sessionFactory.getCurrentSession()
                .createCriteria(Group.class)
                .list();
    }

    public void createGroup(String name) {
        Group group = new Group();
        group.setName(name);
        saveGroup(group);
    }

    public boolean isPresent(String name){
        List<Group> lg = sessionFactory.getCurrentSession()
                .createCriteria(Group.class)
                .list();
        for(Group group:lg){
            if (name.equals(group.getName())){
                return true;
            }
        }
        return false;
    }

    public Integer getIdByName(String name){
        List<Group> lg = sessionFactory.getCurrentSession()
                .createCriteria(Group.class)
                .list();
        for(Group group:lg){
            if (name.equals(group.getName())){
                return group.getId();
            }
        }
        return null;
    }
}
