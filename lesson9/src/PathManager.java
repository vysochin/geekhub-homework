import java.io.File;

/**
 * Created by admin on 27.01.2015.
 */
public class PathManager {

    public String getParentPath(String path){
        return new File(path).getParent();
    }
}
