import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

@WebServlet(name = "testServlet", urlPatterns = {"/fileManager"})
public class FileManager extends HttpServlet {

    String rootFolder = "D:\\GeekHub";
    String ourFolder = rootFolder;
    String parameter = rootFolder;
    TextManager textManager = new TextManager();
    PathManager pathManager = new PathManager();

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        if (!request.getParameterMap().isEmpty()) {
            if (request.getParameter("back").equals("1")) {
                ourFolder = pathManager.getParentPath(ourFolder);
            } else {
                parameter = request.getParameter("file");
                ourFolder = parameter;
            }
        }

        out.println("<html>");
        out.println("<head>");
        out.println("</head>");
        out.println("<body>");
        out.println("<p><h1>" + ourFolder + "</h1>");
        out.println("<form action=\"http://localhost:8080/fileManager?delete="
                    + ourFolder + "\" method=\"POST\">");

        if (!rootFolder.equals(ourFolder)) {
            out.println("<input type=\"submit\" name =\"submit\" value=\"Delete\"></p>");
            out.println("<p><h1><a href=\"http://localhost:8080/fileManager?file=0&back=1\">...</a></h1></p>");
        }

        out.println("</form>");
        out.println("<form action=\"http://localhost:8080/fileManager\" method=\"POST\">");

        if (ourFolder.contains(".txt")) {
            out.println("<textarea name=\"correctText\" cols=\"50\" rows=\"6\">"
                        + textManager.openTxt(ourFolder) + "</textarea>");
            out.println("<p><input type=\"submit\" name=\"submit\" value=\"Save\"></p>");
        } else {
            File mainDir = new File(ourFolder);
            File[] pathFiles = mainDir.listFiles();
            for (File file : pathFiles) {
                out.println("<p><h2><a href=\"http://localhost:8080/fileManager?file="
                            + file.getAbsolutePath() + "&back=0\">"
                            + file.getName() + "</a></h2></p>");
            }
            out.println("<input type=\"text\" name=\"newFile\">");
            out.println("<input type=\"submit\" name=\"submit\" value=\"Create file\">");
            out.println("</form>");
        }
        out.println("</body>");
        out.println("</html>");
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        parameter = request.getParameter("correctText");
        if (parameter != null) {
            textManager.changeTxt(ourFolder, parameter);
            out.println("<p><h3>Text has saved</h3></p>");
            out.println("<a href=\"http://localhost:8080/fileManager?file="
                        + pathManager.getParentPath(ourFolder)
                        + "&back=0\">Return to folder</a>");
        }

        parameter = request.getParameter("newFile");
        if (parameter != null) {
            File newFile = new File(ourFolder + "\\" + parameter);
            if (parameter.contains(".")) {
                newFile.createNewFile();
            } else {
                newFile.mkdir();
            }
            out.println("<meta http-equiv=\"REFRESH\" content=\"1\"; "
                        + "URL=\"http://localhost:8080/fileManager?file="
                        + pathManager.getParentPath(ourFolder) + "&back=0\">");
        }

        parameter = request.getParameter("delete");
        if (parameter != null) {
            File file = new File(parameter);
            file.delete();
            out.println("<p><h3>File has deleted</h3></p>");
            out.println("<a href=\"http://localhost:8080/fileManager?file="
                        + pathManager.getParentPath(ourFolder)
                        + "&back=0\">Return to folder</a>");
        }
    }
}