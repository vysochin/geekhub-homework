import java.io.*;

/**
 * Created by admin on 26.01.2015.
 */
public class TextManager {

    public String openTxt(String path) throws IOException {

        BufferedReader in = new BufferedReader(new FileReader(path));
        String s;
        String text="";
        while ((s = in.readLine()) != null) {
            text += s + "\n";
        }
        in.close();
        return text;
    }

    public void changeTxt(String path, String newText) throws IOException {

        PrintWriter pw = new PrintWriter(new File(path));
        pw.print(newText);
        pw.close();
    }
}
